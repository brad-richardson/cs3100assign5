#ifndef COMMAND_HPP
#define COMMAND_HPP

#include <string>
#include <vector>
#include <sstream>

class Command {
private:
	std::string cmd;
	//args has a duplicate of cmd for easy sending to execvp
	std::vector<std::string> args;
	std::string input;
	std::string output;

public:
	Command(std::string cmd, std::vector<std::string> args = {""}, std::string input="std", std::string output="std")
	{
		this->cmd = cmd;
		this->args = args;
		this->input = input;
		this->output = output;
	}
	Command()
	{
		this->cmd = "";
		this->args = {""};
		this->input = "std";
		this->output = "std";
	}

	std::string getCmd()
	{
		return cmd;
	}
	std::vector<std::string> getArgs()
	{
		return args;
	}
	std::string getInput()
	{
		return input;
	}
	std::string getOutput()
	{
		return output;
	}

	//template found on stack overflow
	bool operator==(const Command& rhs) const
	{ 
		if(cmd == rhs.cmd && args == rhs.args &&
			input == rhs.input && output == rhs.output)
		{
				return true;
		}
		return false;
	}
	bool operator!=(const Command& rhs) const
	{
		return !(*this == rhs);
	}

	std::string toString()
	{
		std::stringstream ss;
		//ss << cmd << " ";
		//args now contains cmd too
		for(auto arg: args)
		{
			ss << arg << " ";
		}
		if(input != "std")
		{
			ss << "< " << input << " ";
		}
		if(output != "std")
		{
			ss << "> " << output << " ";
		}
		return ss.str();
	}
};

#endif