#ifndef SHELL_HPP
#define SHELL_HPP

#include <string>
#include <deque>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <boost/algorithm/string.hpp>
#include "executor.hpp"
#include "command.hpp"

class Shell {
private:
	Executor executor;

public:
	void getInput()
	{
		while(true)
		{
			std::string input;
			std::cout << "> ";
			std::getline(std::cin, input);
			if(input != "")
			{
				processInput(input);
			}
		}
		return;
	}

	//takes input, creates Command and then passes it along
	void processInput(std::string input)
	{
		//for storing all the commands for easy removal from front
		std::deque<Command> commandList;
		//splits by pipes first
		std::vector<std::string> splitByPipes;
		boost::split(splitByPipes, input, boost::is_any_of("|"));
		int count = 1;//for keeping track of where in the vector we are
		for(auto pipedCmd : splitByPipes)
		{
			//now splits by spaces for each command
			std::vector<std::string> splitBySpaces;
			boost::trim_if(pipedCmd, boost::is_any_of(" "));
			boost::split(splitBySpaces, pipedCmd, boost::is_any_of(" "));
			if(splitBySpaces[0] == "exit" || splitBySpaces[0] == "quit" || splitBySpaces[0] == "q")
			{
				_exit(0);
			}
			else
			{
				std::string inputFile, outputFile;
				//finds if a ">", "|" or "<" exists, passes along the filename and erases from args
				auto outputIter = std::find(splitBySpaces.begin(), splitBySpaces.end(), ">");
				if(outputIter != splitBySpaces.end() && outputIter != splitBySpaces.begin())
				{
					outputFile = *(outputIter + 1);//adds 1 so it gets what is right after the >
					splitBySpaces.erase(outputIter + 1);
					splitBySpaces.erase(outputIter);
				}
				else if (splitByPipes.size() > 1 && count < (int)splitByPipes.size())
				{
					outputFile = "pipe";
				}
				else
				{
					outputFile = "std";
				}
				auto inputIter = std::find(splitBySpaces.begin(), splitBySpaces.end(), "<");
				if(inputIter != splitBySpaces.end() && outputIter != splitBySpaces.begin())
				{
					inputFile = *(inputIter + 1);//adds 1 so it gets what is right after the >
					splitBySpaces.erase(inputIter + 1);
					splitBySpaces.erase(inputIter);
				}
				else if (splitByPipes.size() > 1 && count > 1)
				{
					inputFile = "pipe";
				}
				else
				{
					inputFile = "std";
				}
				Command newCommand(splitBySpaces[0], splitBySpaces, inputFile, outputFile);
				commandList.push_back(newCommand);
				//std::cout << " New command: " << newCommand.toString() << std::endl;
				++count;
			}
		}
		executor.history.storeCmd(commandList);
		executor.runCmd(commandList);
		return;
	}
};

#endif