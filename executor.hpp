#ifndef EXECUTOR_HPP
#define EXECUTOR_HPP

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <unistd.h>//for forking and execv
#include <sys/wait.h> //for waitpid
#include <fcntl.h> //for open
#include <chrono>
#include "history.hpp"

const int PIPE_READ = 0;
const int PIPE_WRITE = 1;
const int STDIN = 0;
const int STDOUT = 1;
class Executor 
{
private:
	int prevSeconds = 0;
	int prevMilliseconds = 0;
	int prevMicroseconds = 0;
	int oldPipe[2];
	int newPipe[2];

public:
	History history;

	void runCmd(std::deque<Command> cmdList)
	{
		auto start = std::chrono::system_clock::now();
		for(auto & toRun : cmdList)
		{
			if(toRun.getOutput() == "pipe")
			{
				pipe(newPipe);
			}
			auto pid = fork();
			int status = 0;
			if(pid == -1)
			{
				std::cerr << "Error while forking...";
			}
			else if(pid == 0)
			{
				//setup input and output/piping
				setRedirects(toRun);
				//child process - does commands
				if(toRun.getCmd() == "^" || toRun.getCmd() == "!" || toRun.getCmd() == "history" 
					|| toRun.getCmd() == "ptime")
				{
					runShellCmd(toRun);
					exit(0);
				}
				//creates an array of c strings to pass to execvp
				//+1 to array size so that it ends in a NULL char
				char * args[toRun.getArgs().size() + 1];
				args[0] = (char*)toRun.getCmd().c_str();
				if((int)toRun.getArgs().size() > 1)
				{
					for (int i = 1; i < (int)toRun.getArgs().size(); ++i)
					{
						args[i] = (char*)toRun.getArgs()[i].c_str();
					}
				}
				args[toRun.getArgs().size()] = NULL;
				execvp(args[0], args);
				exit(EXIT_FAILURE);
			}
			else
			{
				//in parent - just waits for child process to finish
				if(toRun.getInput() == "pipe")
				{
					close(oldPipe[PIPE_READ]);
					close(oldPipe[PIPE_WRITE]);
				}
				if(toRun.getOutput() == "pipe")
				{
					oldPipe[PIPE_READ] = newPipe[PIPE_READ];
					oldPipe[PIPE_WRITE] = newPipe[PIPE_WRITE];
				}
				waitpid(pid, &status, 0);
			}
		}
		auto end = std::chrono::system_clock::now();
		prevSeconds = std::chrono::duration_cast<std::chrono::seconds>(end-start).count();
		prevMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
		prevMicroseconds = std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
	}

	void setRedirects(Command cmd)
	{
		auto outputFile = cmd.getOutput();
		auto inputFile = cmd.getInput();
		if(inputFile == "std")
		{
			//do nothing because it is stdin
		}
		else if(inputFile == "pipe")
		{
			dup2(oldPipe[PIPE_READ], STDIN);
			close(oldPipe[PIPE_READ]);
			close(oldPipe[PIPE_WRITE]);
		}
		else
		{
			auto in = open(inputFile.c_str(), O_RDONLY);
			dup2(in, STDIN);
			close(in);
		}
		if(outputFile == "std")
		{
			//do nothing because it is stdout
		}
		else if(outputFile == "pipe")
		{
			close(newPipe[PIPE_READ]);
			dup2(newPipe[PIPE_WRITE], STDOUT);
			close(newPipe[PIPE_WRITE]);
		}
		else
		{
			auto out = open(outputFile.c_str(), O_CREAT|O_TRUNC|O_WRONLY);
			dup2(out, STDOUT);
			close(out);
		}
	}

	//call this if the command is part of the shell
	void runShellCmd(Command cmd)
	{
		if(cmd.getCmd() == "^" && (int)cmd.getArgs().size() > 1)
		{
			auto cmdList = history.getPrevCmd(std::stoi(cmd.getArgs()[1]));
			runCmd(cmdList);
		}
		else if(cmd.getCmd() == "!")
		{
			auto cmdList = history.getLastCmd();
			runCmd(cmdList);
		}
		else if(cmd.getCmd() == "history")
		{
			history.printHistory();
		}
		else if(cmd.getCmd() == "ptime")
		{
			printTime();
		}
	}

	void printTime()
	{
		std::cout << "Time to process previous command: " << std::endl;
		std::cout << "Seconds: " << prevSeconds;
		std::cout << " Microseconds: " << prevMilliseconds;
		std::cout << " Microseconds: " << prevMicroseconds;
		std::cout << std::endl;
	}
};

#endif