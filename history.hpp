#ifndef HISTORY_HPP
#define HISTORY_HPP

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include "command.hpp"

class History
{
private:
	std::vector <std::deque<Command>> hist;
	std::deque<Command> EMPTYCOMMAND;

public:
	//for '^ #' command
	std::deque<Command> getPrevCmd(int id)
	{
		if(id > (int)hist.size())
		{
			return EMPTYCOMMAND;
		}
		return hist[id-1];
	}

	//for use with '!' command
	std::deque<Command> getLastCmd()
	{
		if((int)hist.size() == 0)
		{
			return EMPTYCOMMAND;
		}
		return hist[hist.size()-1];
	}

	void storeCmd(std::deque<Command> command)
	{
		hist.push_back(command);
	}

	void printHistory()
	{
		if((int)hist.size() == 0)
		{
			std::cout << "No previous commands" << std::endl;
			return;
		}
		int count = 1;
		for(auto cmdList : hist)
		{
			std::cout << count << ": ";
			int count2 = 1;
			for(auto cmd : cmdList)
			{
				std::cout << cmd.toString();
				if(cmdList.size() > 1 && !(cmd == cmdList.back()))
				{
					std::cout << "| ";
				}
				++count2;
			}
			std::cout << std::endl;
			++count;
		}
	}
};

#endif